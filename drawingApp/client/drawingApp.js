points = new Meteor.Collection('pointsCollection');
var canvas;

// we use these for drawing more interesting shapes
var lastX=0;
var lastY=0;
var strokeWidth = 1;
var thickness=Session.get("sliderVal");

var strokeColor = "black";

Meteor.startup( function() {
  canvas = new Canvas();

  Deps.autorun( function() {
    var data = points.find({}).fetch();

    if (canvas) {
      canvas.draw(data);
    }
  });
});

Template.tools.events({

  "click button.clear": function (event) {
    Meteor.call('clear', function() {
      canvas.clear();
    });
  },

  //choose a color. Initialise the last vals, otherwise a stray line will appear.
"click .save-work" : function(){
    $(".export").remove()
    var svg = document.getElementById('mySvg');
    svg.setAttribute('width',"100%");
    var svgData = new XMLSerializer().serializeToString( svg );
    var canvas = document.createElement("canvas");
    var svgSize = svg.getBoundingClientRect();
    canvas.width = svgSize.width*3;
    canvas.height = svgSize.height*3;
    var ctx = canvas.getContext("2d");
    var img = document.createElement( "img" );
    img.setAttribute( "src", "data:image/svg+xml;base64," + btoa( svgData ) );

    img.onload = function() {
      ctx.drawImage( img, 100, 100, canvas.width,canvas.height );
      var link =canvas.toDataURL("image/png")
      $(".save-work-wrapper").append("<div class='export'><a href="+link+" target='_blank'>Click me to export your canvas!</a></div>")
    };
    svg.setAttribute('width','100%');
  },

  "click button.clear": function (event) {
    Meteor.call('clear', function() {
      canvas.clear();
    });
  },

  //choose a color. Initialise the last vals, otherwise a stray line will appear.

  "click .colors button":function(event){
    var width = $(".color-picked").css("width");
    $(".color-picked").css("height", width)
    var color = event.target.style.backgroundColor;
    $(".color-picked").css("background-color", color);
    if(color === "rgb(255, 255, 255)"){
      $(".color-picked").css('border','1px solid #000000');
    }
    else{
      $(".color-picked").css('border','');
    }


    //var color = this.
    //$("colorPicked").css("background")
  },
  "click button.red": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#ef4566";

  },

  "click button.black": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#fecea8";
  },

  "click button.white": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#2a363b";
  },/*ready*/

  "click button.blue": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#5a5050";
  },
  "click button.silver": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#e84a5f";
  },
  "click button.gray": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#ff847c";
  },
  "click button.brown": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#f66a9a";
  },
  "click button.yellow": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#f9cdae";
  },
  "click button.green": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#a2d4ab";
  },
  "click button.olive": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#c8c8a9";
  },
  "click button.lime": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#83ae9b";
  },
  "click button.aqua": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#3eaca8";
  },
  "click button.teal": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#547a82";
  },
  "click button.navy": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#f8b195";
  },
  "click button.fushia": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#f67280";
  },
  "click button.purple": function () {
    lastX=0;
    lastY=0;
    strokeColor = "#c06c84";
  },

  "click button.thicker": function () {

    thickness+=1;

  },

  "click button.thinner": function () {
    
    if (thickness > 0) {
      thickness-=1;
    }
  },
});

Template.tools.helpers({
  sliderVal : function(){
    var slider = points.findOne();
    if (slider && slider.w !== undefined) { 
        Template.instance().$('#slider').data('uiSlider').value(slider.w);
        return slider.w;
      }
  }
});

Template.tools.onRendered(function(){
    var handler = _.throttle(function(event, ui){
      var val = points.findOne({});
      console.log(val)
      thickness= ui.value
      Session.set("sliderVal", thickness);
      points.update({_id:val._id},{$set: {w: thickness}})
     }, 50, { leading: false });

    if (!this.$('#slider').data('uiSlider')) {
      $('#slider').slider({
      slide:handler,
      min:0,
      max:100,
    });
    }
  });

var markPoint = function() {

  var offset = $('#canvas').offset();

// In the first frame, lastX and lastY are 0.
// This means the line gets drawn to the top left of the screen
// Which is annoying, so we test for this and stop it happening.

      if (lastX==0) {// check that x was something not top-left. should probably set this to -1
        lastX = (event.pageX - offset.left);
        lastY = (event.pageY - offset.top);
      }
      points.insert({
        //this draws a point exactly where you click the mouse
      // x: (event.pageX - offset.left),
      // y: (event.pageY - offset.top)});


        //We can do more interesting stuff
        //We need to input data in the right format
        //Then we can send this to d3 for drawing


        //1) Algorithmic mouse follower
      // x: (event.pageX - offset.left)+(Math.cos((event.pageX/10  ))*30),
      // y: (event.pageY - offset.top)+(Math.sin((event.pageY)/10)*30)});

        //2) draw a line - requires you to change the code in drawing.js
        x: (event.pageX - offset.left),
        y: (event.pageY - offset.top),
        x1: lastX,
        y1: lastY,
        // We could calculate the line thickness from the distance
        // between current position and last position
        //w: 0.05*(Math.sqrt(((event.pageX - offset.left)-lastX) * (event.pageX - offset.left)
        //  + ((event.pageY - offset.top)-lastY) * (event.pageY - offset.top))),
        // Or we could just set the line thickness using buttons and variable
        w: thickness,
        // We can also use strokeColor, defined by a selection
        c: strokeColor,


      }); // end of points.insert()

        lastX = (event.pageX - offset.left);
        lastY = (event.pageY - offset.top);

}

Template.canvas.events({
  'click': function (event) {
    markPoint();
  },
  'mousedown': function (event) {
    Session.set('draw', true);
  },
  'mouseup': function (event) {
    Session.set('draw', false);
    lastX=0;
    lasyY=0;
  },
  'mousemove': function (event) {
    if (Session.get('draw')) {
      markPoint();
    }
  }
});
